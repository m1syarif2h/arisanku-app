<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeatailPeminjaman extends Model
{
    protected $table = 'detail_peminjaman';
     protected $fillable = [
        'id_inventaris','id_peminjaman','jumlah'
    ];
}
