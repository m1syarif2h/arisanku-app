<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
     protected $fillable = [
        'id_pegawai','tanggal_pinjam','tanggal_kembali','status'
    ];
}
