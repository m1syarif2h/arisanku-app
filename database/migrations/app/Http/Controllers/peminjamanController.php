<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\Pegawai;
use App\DeatailPeminjaman;
use App\Inventaris;

class peminjamanController extends Controller
{
    public function index(){
    	$no = 1;
    	$data = Peminjaman::
    	join('pegawai','pegawai.id','=','peminjaman.id_pegawai')
    	->join('detail_peminjaman','detail_peminjaman.id_peminjaman','=','peminjaman.id')
    	->join('inventaris','inventaris.id','=','detail_peminjaman.id_inventaris')
    	->select('peminjaman.*','inventaris.nama as nama_barang','pegawai.nama as nama_pegawai','detail_peminjaman.jumlah as jumlah')
    	->get();
    	return view('peminjaman',compact('data','no'));
  //   	$no=1;
		// $data = Peminjaman::
		// join('detail_peminjaman','peminjaman.id','=','detail_peminjaman.id_peminjaman')
		// ->join('pegawai','pegawai.id','=','peminjaman.id_pegawai')
		// ->join('inventaris','inventaris.id','=','detail_peminjaman.id_inventaris')
		// ->select('peminjaman.*','pegawai.nip as nip','pegawai.nama as nama'
		// ,'inventaris.nama as nama_barang','detail_peminjaman.jumlah')
		// ->orderBy('peminjaman.id','desc')
		// ->get();
		// return view('peminjaman',compact('data','no'));
    }
    public function tambahView(){
    	return view('form_tambah_pem');
    }
    public function tambah(Request $request){
    	$nip = $request->nip;
    	$nama_peminjam = $request->nama_peminjam;
    	$nama_barang = $request->nama_barang;
    	$jumlah = $request->jumlah;

    	$pegawai = Pegawai::where('nip',$nip)->orderBy('id','desc')->first();

    	$data = new Peminjaman;
    	$data->id_pegawai = $pegawai->id;
    	$data->tanggal_peminjaman = date("Y-m-d");
    	$data->status = 'Belum Dikembalikan';
    	$data->save();

    	$peminjaman = Peminjaman::orderBy('id','desc')->first();
    	$inventaris = Inventaris::where('nama',$nama_barang)->first();
    }
}
