<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Inventaris;
use App\Jenis;
use App\Ruang;
use App\Petugas;
class inventarisController extends Controller
{
    public function index(){
    	$data = Inventaris::
    	join('jenis','jenis.id','=','inventaris.id_jenis')
    	->join('ruang','ruang.id','=','inventaris.id_ruang')
    	->join('petugas','petugas.id','=','inventaris.id_petugas')
    	->select('inventaris.*','jenis.kode as jenis','ruang.kode as ruang','petugas.nama as petugas')
    	->get();
    	$no = 1;
    	return view('inventaris',compact('data','no'));
    }
    public function tambahView(){
    	$data1 = Jenis::get();
    	$data2 = Ruang::get();
    	return view('form_tambah_inv',compact('data1','data2'));
    }
    public function tambahLog(Request $request){
    	$data1 = Jenis::get();
    	$data2 = Ruang::get();
    	$insert = new Inventaris;
    	$insert->kode = $request->kode;
    	$insert->nama = $request->nama;
    	$insert->id_jenis = $request->jenis;
    	$insert->id_ruang = $request->ruang;
    	$insert->id_petugas = Session::get('id_petugas');
    	$insert->stok = $request->stok;
    	$insert->kondisi = $request->kondisi;
    	$insert->keterangan = $request->keterangan;
    	$insert->tanggal_regis = date("Y-m-d");
    	$insert->save();
    	return redirect('/inventaris');

    }
    public function editView($id){
    	$data = Inventaris::
    	where('id',$id)
    	->first();
    	$data1 = Jenis::get();
    	$data2 = Ruang::get();
    	return view('form_edit_inv',compact('data','data1','data2'));
    }
    public function editLog($id, Request $request){
    	$update = Inventaris::find($id);
    	$update->kode = $request->kode;
    	$update->nama = $request->nama;
    	$update->id_jenis = $request->jenis;
    	$update->id_ruang = $request->ruang;
    	$update->stok = $request->stok;
    	$update->kondisi = $request->kondisi;
    	$update->keterangan = $request->keterangan;
    	$update->save();
    	return redirect('/inventaris');
    }
    public function tambahStok($id){
    	$data = Inventaris::where('id',$id)->first();
    	$stokakhir = $data->stok + 1;
    	$update = Inventaris::find($id);
    	$update->stok = $stokakhir;
    	$update->save();
    	return redirect('/inventaris');
    }
    public function kurangStok($id){
    	$data = Inventaris::where('id',$id)->first();
    	$stokakhir = $data->stok - 1;
    	$update = Inventaris::find($id);
    	$update->stok = $stokakhir;
    	$update->save();
    	return redirect('/inventaris');
    }
}
