<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Petugas;
use App\Pegawai;
class loginController extends Controller
{
    public function indexPetugas(){
    	return view('loginPetugas');
    }
    public function postPetugas(Request $request){
    	$username = $request->username;
    	$password = md5($request->password);
    	$data = Petugas::where('username',$username)->first();
    	if($data != null){
    		if ($data->password == $password) {
    			if($data->id_level == 1){
    				Session::put('loginadmin',TRUE);
    				Session::put('id_petugas',$data->id);
    				return redirect('/beranda');
    			}else{
    				Session::put('loginoperator',TRUE);
    				Session::put('id_petugas',$data->id);
    				return redirect('/beranda');
    			}
    		}
    		else{
    			return view('loginPetugas')->with('alert','Password Salah');
    		}
    	}
    	else{
    			return view('loginPetugas')->with('alert','Username Tidak Ditemukan');
    		}
    }
    public function indexPegawai(){
    	return view('loginPegawai');
    }
    public function postPegawai(Request $request){
    	$username = $request->username;
    	$password = md5($request->password);
    	$data = Pegawai::where('username',$username)->first();
    	if($data != null){
    		if ($data->password == $password) {
    				Session::put('loginpeminjam',TRUE);
    				Session::put('id_pegawai',$data->id);
    				return redirect('/beranda');
    		}
    		else{
    			return view('loginPetugas')->with('alert','Password Salah');
    		}
    	}
    	else{
    			return view('loginPetugas')->with('alert','Username Tidak Ditemukan');
    		}
    }
    public function logout(){
    	Session::destroy();
    	return view('loginhome');
    }
}
