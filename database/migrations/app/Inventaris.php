<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Inventaris extends Model
{
    protected $table = 'inventaris';
     protected $fillable = [
        'id_petugas','id_jenis','id_ruang','nama','kondisi','stok','tanggal_regis','keterangan'
    ];
}
