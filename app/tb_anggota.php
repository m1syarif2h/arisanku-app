<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb_anggota extends Model
{
  protected $table = 'tb_anggota';
   protected $fillable = [
      'nama','alamat','tgl_lahir','status_bayar','status_menang'
  ];
}
