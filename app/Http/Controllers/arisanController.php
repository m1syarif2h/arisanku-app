<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\tb_anggota;
class arisanController extends Controller
{
    public function index(){
    	$data = tb_anggota::
    	select('tb_anggota.*')
    	->get();
    	$no = 1;
    	return view('arisan',compact('data','no'));
    }

    public function index2(){
    	$data = tb_anggota::
    	select('tb_anggota.*')->
      where("status_bayar","sudah bayar")
      ->where("status_menang","belum menang")
      ->get();
    	$no = 1;
    	return view('kocokarisan',compact('data','no'));
    }

    public function tambahView(){
    	$data1 = tb_anggota::get();

    	return view('form_tambah_arisan',compact('data1'));
    }

    public function tambahLog(Request $request){

      $data1 = tb_anggota::get();
    	$insert = new tb_anggota;
      $insert->id = $request->kode;
    	$insert->nama = $request->nama;
      $insert->alamat = $request->alamat;
    	$insert->status_bayar = 'belum bayar';
      $insert->status_menang ='belum menang';
    	$insert->save();
    	return redirect('/');
    }

    public function editView($id){
    	$data = tb_anggota::
    	where('id',$id)
    	->first();
    	return view('form_edit_arisan',compact('data'));
    }

    public function editLog($id, Request $request){

      $update = tb_anggota::find($id);

    	$update->nama = $request->nama;
      $update->alamat = $request->alamat;
    	$update->status_bayar = $request->bayar;
      $update->status_menang =$request->menang;
    	$update->save();
    	return redirect('/');
    }

    public function hapusLog($id){
            tb_anggota::where('id',$id)->delete();

    	return redirect('/');
    }

    public function bayarlog($id, Request $request){
      $update = tb_anggota::find($id);
        $update->status_bayar = 'sudah bayar';
        $update->save();
          return redirect('/');
    }

    public function Kocok(){
      $data = tb_anggota::
      select('tb_anggota.*')
      ->where("status_bayar","sudah bayar")
      ->where("status_menang","belum menang")
      ->get();
      $no = 1;
      $DataKocokan = tb_anggota::
      where("status_bayar","sudah bayar")
    ->where("status_menang","belum menang")->inRandomOrder()->first();

    return view('kocokarisan0',compact('DataKocokan','data','no'));

         }
  public function menanglog($id, Request $request){

             $update = tb_anggota::find($id);
             $update->status_menang = 'sudah menang';
             $update->save();
        return redirect('/');
         }

    }
