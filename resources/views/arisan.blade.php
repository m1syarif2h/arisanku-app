@extends('layout.master')

@section('title','Beranda')
@section('konten')
	<div class="container" style="">
		<h1>Manage Data Arisan</h1></h1><br><br>
		<div class="row">
			<a href="{{url('/tambahView')}}" class="col-sm"><button class="btn btn-success">Tambah</button></a><br>
						<a href="{{url('/kocokarisan')}}" class="col-sm"><button class="btn btn-warning">Kocok arisan</button></a>

		</div><br>
		<table class="table table-stripped">
			<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Status Bayar</th>
				<th>Status menang</th>
				<th>Aksi</th>
			</tr>
			</thead>
			<tbody>

				@foreach($data as $data)
				<tr>
					<td>{{$no++}}</td>
					<td>{{$data->nama}}</td>
					<td>{{$data->alamat}}</td>
					<td>{{$data->status_bayar}}</td>
					<td>{{$data->status_menang}}</td>
					<td>

					<a href="{{url('/editView/'.$data->id)}}"><button class="btn btn-info">Ubah</button></a>
					<a href="{{url('/bayar/'.$data->id)}}"><button class="btn btn-success">bayar</button></a>
					<a href="{{url('/hapus/'.$data->id)}}"><button class="btn btn-danger">hapus</button></a>

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
@endsection
