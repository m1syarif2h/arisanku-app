@extends('layout.master')

@section('title','Tambah')
@section('konten')
	<div class="container">
		<h1>Edit Barang</h1>
			<form method="post" action="{{url($data->id.'/editLog')}}">
				@csrf
				<div class="form-group">
					<label class="control-label">Nama</label>
					<input type="text" name="nama" value="{{$data->nama}}" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">alamat</label>
					<input type="text" name="alamat" value="{{$data->alamat}}" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">status bayar</label>
					<p>
						isi dengan sudah bayar / belum bayar
					</p>
					<input type="text" name="bayar" value="{{$data->status_bayar}}" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">status menang</label>
					<p>
						isi dengan sudah menang / belum menang
					</p>
					<input type="text" name="menang" value="{{$data->status_menang}}" class="form-control">
				</div>
				<div class="form-group">
					<button class="btn btn-success" type="submit">Simpan</button>
				</div>
			</form>
	</div>
@endsection
