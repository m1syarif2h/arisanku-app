<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.js')}}">
	<style type="text/css">
		body{
			font-family: Times New Roman;
			background-color: #eee;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="/" class="navbar-brand">ARISANKU</a>
			</div>

		</div>
	</nav>
	@yield('konten')
</body>
</html>

<!-- illuminate/Support/Facades/Session -->
