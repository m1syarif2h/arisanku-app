@extends('layout.master')

@section('title','Tambah')
@section('konten')
	<div class="container">
		<h1>Tambah Anggota</h1>
			<form method="post" action="{{url('/tambahInvLog')}}">
				@csrf
				<div class="form-group">
					<label class="control-label">kode</label>
					<input type="text" name="kode" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Nama</label>
					<input type="text" name="nama" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Alamat</label>
					<input type="text" name="alamat" class="form-control">
				</div>
				<div class="form-group">
					<button class="btn btn-success" type="submit">Simpan</button>
				</div>
			</form>
	</div>
@endsection
