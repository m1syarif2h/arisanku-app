@extends('layout.master')

@section('title','kocok arisan')
@section('konten')
	<div class="container" style="">
		<h1>Data Arisan</h1></h1><br><br>
		<div class="row">
						<a href="/hasil" class="col-sm"><button class="btn btn-warning">Mulai Kocok</button></a>

		</div><br>
		<table class="table table-stripped">
			<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Status menang</th>
			</tr>
			</thead>
			<tbody>

				@foreach($data as $data)
				<tr>
					<td>{{$no++}}</td>
					<td>{{$data->nama}}</td>
					<td>{{$data->status_menang}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
@endsection
