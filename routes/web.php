<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'arisanController@index');
Route::get('/kocokarisan', 'arisanController@index2');
Route::get('/hasil', 'arisanController@kocok');

Route::get('/tambahView', 'arisanController@tambahView');
Route::post('/tambahInvLog', 'arisanController@tambahLog');

Route::get('/hapus/{id}', 'arisanController@hapusLog');

Route::get('/bayar/{id}', 'arisanController@bayarLog');

Route::get('/menang/{id}', 'arisanController@menanglog');


Route::get('/editView/{id}', 'arisanController@editView');
Route::post('{id}/editLog', 'arisanController@editLog');
Route::get('/tambah/{id}', 'arisanController@tambahStok');
Route::get('/kurang/{id}', 'arisanController@kurangStok');
